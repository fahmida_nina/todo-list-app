import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DetailsTodoComponent } from './details-todo/details-todo.component';


const lroutes: Routes = [
    { path: '', component: DetailsTodoComponent,  pathMatch: 'full'},
 
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    [RouterModule.forChild(lroutes)],
  ],
  exports:[
    RouterModule
  ]
})
export class LazyRoutingModule { }
