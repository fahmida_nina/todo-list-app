import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsTodoComponent } from './details-todo/details-todo.component';
import { LazyRoutingModule } from './lazy-routing.module';



@NgModule({
  declarations: [DetailsTodoComponent],
  imports: [
    CommonModule,
    LazyRoutingModule,
    
  ],
  exports:[DetailsTodoComponent]
})
export class LazyDetailsModule { }
