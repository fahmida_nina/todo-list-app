
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Todo } from 'src/app/entity/todo';
import { TodoService } from 'src/app/service/todo.service';

@Injectable()
export class ChangeHelper {
  currentIndex: Observable<Todo[]>;
  currentIndexObs: BehaviorSubject<Observable<Todo[]>>;
  currentIndexObsArr: BehaviorSubject<Observable<Todo[]>>[];

  constructor(private todoService: TodoService) {
    this.currentIndex= this.todoService.getTodos();
    // this.currentIndex = this.listFilter ? this.doFilter(this.listFilter) : this.todoService.getTodos();
    console.log(this.currentIndex);
    this.currentIndexObs = new BehaviorSubject<Observable<Todo[]>>(this.currentIndex);
    
  }

  changeIndex(index: Observable<Todo[]>) {
    this.currentIndexObs.next(index);
  }

  // _listFilter = '';

  // get listFilter(): string {
  //   return this._listFilter;
  // }
  // set listFilter(value: string) {
  //   this._listFilter = value;
  //   this.currentIndex = this.listFilter ? this.doFilter(this.listFilter) : this.todoService.getTodos();
  // }


  // doFilter(filterBy: string) {
  //   let todos = this.todoService.getFilterTodos(filterBy)
  //   console.log(todos);
  //   this.currentIndexObs.next(todos)
  //   return todos;

  // }



}