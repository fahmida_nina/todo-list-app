export class Todo {
    id:number;
    title: string;
    location: string;
    contact: string;
    note: string;
}
