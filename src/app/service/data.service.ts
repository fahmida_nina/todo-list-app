import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api'

@Injectable({
  providedIn: 'root'
})


export class DataService implements InMemoryDbService {

  constructor() { }
  createDb() {

    let todos = [
      { id: 1, title: 'T', location: 'Dhaka', contact: '15987', note: 'No Note' },
      { id: 2, title: 'G', location: 'Chittagong', contact: '15987', note: 'No Note' },
      { id: 3, title: 'P', location: 'Rangpur', contact: '15987', note: 'No Note' },
      { id: 4, title: 'R', location: 'Dhaka', contact: '15987', note: 'No Note' },

    ];

    return { todos };

  }
}