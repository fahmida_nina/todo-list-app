import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Todo } from '../entity/todo';
import { throwError, Observable, from } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import {filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  SERVER_URL: string = "http://localhost:8080/api/";


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', JSON.stringify(error));
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`);
      console.log(JSON.stringify(error.error));
    }
    // return an observable with a user-facing error message
    if (error.message != null || error.message != "") {
      return throwError(error);
    } else {
      return throwError('Something bad happened; please try again later.');
    }

  }

  constructor(private httpClient: HttpClient) { }


  public getTodos() {
    return this.httpClient.get<Todo[]>(this.SERVER_URL + 'todos');
  }

  public getFilterTodos(filterBy: string) {
    let data;

  }

  public getTodo(todoId) {
    return this.httpClient.get(`${this.SERVER_URL + 'todos'}/${todoId}`);
  }

  createTodo(todo: Todo) {
    return this.httpClient.post<Todo>(`${this.SERVER_URL + 'todos'}`, todo).pipe(
      //catchError((error: any) => Observable.throw(error))
    );
  }


}
