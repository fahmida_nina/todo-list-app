import { Component, OnInit } from '@angular/core';
import { Todo } from '../entity/todo';
import { throwError, Observable, from } from 'rxjs';
import { TodoService } from '../service/todo.service';
import { ChangeHelper } from 'src/Helper/change-helper';
import { filter, map } from 'rxjs/operators';


@Component({
  selector: 'app-search-todo-list',
  templateUrl: './search-todo-list.component.html',
  styleUrls: ['./search-todo-list.component.css']
})
export class SearchTodoListComponent implements OnInit {

  constructor(private todoService: TodoService, private _passVarHelper: ChangeHelper, ) { }

  ngOnInit(): void {
  }

  search(value: string){

    let currentIndex = this.todoService.getTodos().pipe(
      map(result =>
        result.filter(one => one.title.toLocaleLowerCase() === value.toLocaleLowerCase())
      ));
   this._passVarHelper.changeIndex(currentIndex);
  }
}
