import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Todo } from '../entity/todo';
import { TodoService } from '../service/todo.service';
import { MasterComponent } from '../master/master.component';
import { Observable } from 'rxjs';
import { ChangeHelper } from 'src/Helper/change-helper';


@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.css']
})
export class CreateTodoComponent implements OnInit {

  todoForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private todoService: TodoService, private _passVarHelper: ChangeHelper) {

    this.todoForm = this.formBuilder.group({
      id: [],
      title: [''],
      location: [''],
      contact: [''],
      note: [''],

    });

  }


  ngOnInit(): void {
  }


  async saveTodo() {
    let data: Todo = this.todoForm.value;

    await this.todoService.createTodo(data)
      .subscribe((res: Todo) => {
        let currentIndex = this.todoService.getTodos();
        this._passVarHelper.changeIndex(currentIndex);

        // alert('save successfully  ' + res.title)
      }, err => {
        console.log(err);
      });

   
  }



}
