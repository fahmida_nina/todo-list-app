import { Component, OnInit, AfterViewInit, OnChanges, Input } from '@angular/core';
import { TodoService } from '../service/todo.service';
import { Todo } from '../entity/todo';
import { Observable } from 'rxjs';
import { ChangeHelper } from 'src/Helper/change-helper';
import { Router } from '@angular/router';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.css']
})
export class MasterComponent implements OnInit {


  constructor(private todoService: TodoService, private _passVarHelper: ChangeHelper, private router: Router) {

  }
  todos: Todo[] = [];
  observable: Observable<Todo[]>;
  filtered: any = [];

  ngOnInit(): void {

    this.loadIndex();
    this.GetTodos();
  }


  // _listFilter = '';
  // get listFilter(): string {
  //   return this._listFilter;
  // }
  // set listFilter(value: string) {
  //   this._listFilter = value;
  //   this.filtered = this.listFilter ? this.doFilter(this.listFilter) : this.observable;
  // }


  // doFilter(filterBy: string): Todo[] {
  //   filterBy = filterBy.toLocaleLowerCase();
  //   return this.todos.filter((todo: Todo) =>
  //     todo.title.toLocaleLowerCase().indexOf(filterBy) !== -1);
  // }

  loadIndex() {
    this._passVarHelper.currentIndexObs.subscribe(
      response => {
        if (response) {
          this.observable = response;
        }
        
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  async GetTodos() {
    this.todoService.getTodos().subscribe((data: Todo[]) =>
     {
        this.todos = data;

     })
  }




}
