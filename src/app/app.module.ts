import { BrowserModule } from '@angular/platform-browser';
import { NgModule, enableProdMode } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { MasterComponent } from './master/master.component';
import { CreateTodoComponent } from './create-todo/create-todo.component';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataService } from './service/data.service';
import { ChangeHelper } from 'src/Helper/change-helper';
import { TodoService } from './service/todo.service';
import { SearchTodoListComponent } from './search-todo-list/search-todo-list.component';

enableProdMode();


@NgModule({
  declarations: [
    AppComponent,
    MasterComponent,
    CreateTodoComponent,
    SearchTodoListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule, 
    RouterModule,
    InMemoryWebApiModule.forRoot(DataService)
  ],
  providers: [ChangeHelper, TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
