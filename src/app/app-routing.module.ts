import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterComponent } from './master/master.component';
import { LazyDetailsModule } from 'src/lazy-modules/lazy-details.module';


const routes: Routes = [
  { path: '', component: MasterComponent,  pathMatch: 'full'},
  { path: 'd', loadChildren: './../lazy-modules/lazy-details.module#LazyDetailsModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
